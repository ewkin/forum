import React from 'react';
import {Route, Switch} from "react-router-dom";
import {Container, CssBaseline} from "@material-ui/core";

import AppToolBar from "./components/UI/AppToolBar/AppToolBar";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Posts from "./containers/Posts/Posts";
import NewPost from "./containers/NewPost/NewPost";
import Comments from "./containers/Comments/Comments";


const App = () => {


  return (
    <>
      <CssBaseline/>
      <header><AppToolBar/></header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <Route path="/" exact component={Posts}/>
            <Route path="/post/new" component={NewPost}/>
            <Route path="/register" component={Register}/>
            <Route path="/post/:id" component={Comments}/>
            <Route path="/login" component={Login}/>
            <Route render={() => <h1>Not found</h1>}/>
          </Switch>
        </Container>
      </main>
    </>
  );
};

export default App;