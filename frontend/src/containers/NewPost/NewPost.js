import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import PostForm from "../../components/PostForm/PostForm";
import {createPosts} from "../../store/actions/postsActions";
import {Alert, AlertTitle} from "@material-ui/lab";

const NewPost = () => {

  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const error = useSelector(state => state.posts.postsError);

  const onPostFormSubmit = async postData => {
    await dispatch(createPosts(postData, user.token));
  };


  return (
    <Grid container direction="column">
      <Grid item xs>
        <Typography variant='h4'>
          New post
        </Typography>
        {error && (
          <Grid item xs>
            <Alert severity="error">
              <AlertTitle>Error</AlertTitle>
              {error.message || error.global}
            </Alert>
          </Grid>
        )}
      </Grid>
      <Grid item xs>
        <PostForm onSubmit={onPostFormSubmit}/>
      </Grid>
    </Grid>
  );
};

export default NewPost;