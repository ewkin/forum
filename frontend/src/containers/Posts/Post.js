import React from 'react';
import moment from 'moment';
import PropTypes from "prop-types";
import {Card, CardContent, CardMedia} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {apiURL} from "../../config";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import imageMessageIcon from '../../assets/images/messageIcon.png';


const useStyles = makeStyles({
  root: {
    maxWidth: '100%',
    marginBottom: '10px',
    padding: 20
  },
  media: {
    width: '100px',
    height: 100,
    backgroundSize: "contain"

  },
});


const Post = ({title, description, datetime, image, username, readMore}) => {
  const classes = useStyles();

  let cardImage = imageMessageIcon;
  if (image) {
    cardImage = apiURL + '/uploads/' + image;
  }

  let button = null;

  if (readMore) {
    button = <Button onClick={readMore}>Comments</Button>;
  }


  return (
    <Card direction="row" className={classes.root}>
      <Grid item container direction="row" justify="flex-start" alignItems="flex-end">
        <CardMedia
          image={cardImage}
          title={title}
          className={classes.media}
        />
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
            {moment(datetime).format('MMMM Do YYYY, h:mm a')} by {username}
          </Typography>
          <Typography gutterBottom variant="h5" component="h2">
            {title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {description}
          </Typography>
        </CardContent>
        <Grid item>
          {button}
        </Grid>
      </Grid>
    </Card>
  );
};


Post.propTypes =
  {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    datetime: PropTypes.string.isRequired,
    readMore: PropTypes.func,
    image: PropTypes.string
  };

export default Post;