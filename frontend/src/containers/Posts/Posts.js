import React, {useEffect} from 'react';
import {clearErrors} from "../../store/actions/usersActions";
import {useDispatch, useSelector} from "react-redux";
import history from "../../history";
import {fetchPosts} from "../../store/actions/postsActions";
import {historyPush} from "../../store/actions/historyActions";
import Post from "./Post";
import {CircularProgress} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";


const Posts = () => {
  const dispatch = useDispatch();

  history.listen((location) => {
    if (location.pathname === '/login' || location.pathname === '/register') {
      dispatch(clearErrors());
    }
  });

  const loading = useSelector(state => state.posts.postsLoading);
  const posts = useSelector(state => state.posts.posts);

  const readMore = async id => {
    dispatch(historyPush('/post/' + id));
  };

  useEffect(() => {
    dispatch(fetchPosts());
  }, [dispatch]);


  return (
    <>
      {loading ? (
        <Grid container justify='center' alignContent="center">
          <Grid item>
            <CircularProgress/>
          </Grid>
        </Grid>) : posts.map(post => (
        <Post
          key={post._id}
          id={post._id}
          title={post.title}
          description={post.description}
          datetime={post.datetime}
          readMore={() => readMore(post._id)}
          username={post.user.username}
          image={post.image}
        />
      ))}
    </>
  );
};

export default Posts;