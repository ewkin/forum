import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {fetchPost} from "../../store/actions/postsActions";
import Grid from "@material-ui/core/Grid";
import {Card, CircularProgress, TextField} from "@material-ui/core";
import Post from "../Posts/Post";
import Comment from "./Comment";
import Button from "@material-ui/core/Button";
import {createComments, fetchComments} from "../../store/actions/commentsActions";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles({
  root: {
    maxWidth: '100%',
    marginBottom: '10px',
    padding: 20
  },
  media: {
    width: '40%',
    height: 100,
    backgroundSize: "contain"

  },
});

const Comments = () => {

  const classes = useStyles();
  const dispatch = useDispatch();
  const loading = useSelector(state => state.posts.postsLoading);
  const post = useSelector(state => state.posts.post);
  const user = useSelector(state => state.users.user);
  const comments = useSelector(state => state.comments.comments);
  const {id} = useParams();

  const [comment, setComment] = useState({
    comment: ''
  });

  useEffect(() => {
    dispatch(fetchComments(id))
    dispatch(fetchPost(id));
  }, [dispatch, id]);

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setComment(prevState => ({
      ...prevState, [name]: value
    }));
  };

  const submitFormHandler = async e => {
    e.preventDefault();
    const data = comment;
    data.post = id;
    await dispatch(createComments(data, user.token));
    setComment({comment: ''})
  }

  return (
    <>
      {loading ? (
        <Grid container justify='center' alignContent="center">
          <Grid item>
            <CircularProgress/>
          </Grid>
        </Grid>) : post.map(post => (
        <Post
        key={post._id}
        id={post._id}
        title={post.title}
        description={post.description}
        datetime={post.datetime}
        username={post.user.username}
        image={post.image}
        />
        ))}
      {comments.map(comm => (
        <Comment
          key={comm._id}
          author={comm.user.username}
          comment={comm.comment}
        />

      ))}
      {!user ? (null) :
        (<Card className={classes.root}>
            <form onSubmit={submitFormHandler}>
              <Grid container direction="column" spacing={2}>
                <Grid item xs>
                  <TextField
                    fullWidth
                    multiline
                    rows={3}
                    required
                    variant="outlined"
                    id="comment"
                    label="comment"
                    name="comment"
                    value={comment.comment}
                    onChange={inputChangeHandler}/>
                </Grid>
                <Grid item xs>
                  <Button type="submit" color="primary" variant="contained">
                    Add
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Card>
        )}
    </>
  );
};

export default Comments;