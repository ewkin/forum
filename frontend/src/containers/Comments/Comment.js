import React from 'react';
import {Card, CardContent} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles({
  root: {
    maxWidth: '95%',
    marginRight: '5%',
    marginBottom: '10px',
    padding: 20
  },
});


const Comment = ({author, comment}) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {author}
        </Typography>
      </CardContent>
      <CardContent>
        <Grid item container direction="row" justify="space-between" alignItems="center">
          <Grid item>
            <Typography variant="body2" color="textSecondary" component="p">
              {comment}
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default Comment;