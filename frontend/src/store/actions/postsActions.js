import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';
import {historyPush} from "./historyActions";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';

export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';

export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';


export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const fetchPostsFailure = error => ({type: FETCH_POSTS_FAILURE, error});

export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});

export const fetchPostSuccess = post => ({type: FETCH_POST_SUCCESS, post});


export const fetchPosts = () => {
  return async dispatch => {
    try {
      dispatch(fetchPostsRequest());
      const response = await axiosApi.get('/posts');
      dispatch(fetchPostsSuccess(response.data));
    } catch (e) {
      dispatch(fetchPostsFailure(e.response.data));
      NotificationManager.error('Could not fetch posts')
    }
  }
};

export const fetchPost = id => {
  return async dispatch => {
    try {
      dispatch(fetchPostsRequest());
      const response = await axiosApi.get('/posts/' + id);
      console.log('hey ')
      dispatch(fetchPostSuccess(response.data));
    } catch (e) {
      dispatch(fetchPostsFailure(e.response.data));
      NotificationManager.error('Could not fetch posts')
    }
  }
};

export const createPosts = (data, token) => {
  return async dispatch => {
    try {
      dispatch(fetchPostsRequest());
      const response = await axiosApi.post('/posts', data, {headers: {'Authorization': token}});
      dispatch(createPostSuccess());
      dispatch(historyPush('/'));
    } catch (error) {
      if (error.response && error.response.data) {
        dispatch(fetchPostsFailure(error.response.data));
      } else {
        dispatch(fetchPostsFailure({global: 'No internet'}));
      }
    }
  }
};