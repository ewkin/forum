import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});
export const fetchCommentsFailure = () => ({type: FETCH_COMMENTS_FAILURE});


export const fetchComments = (id) => {
  return async dispatch => {
    try {
      dispatch(fetchCommentsRequest());
      const response = await axiosApi.get('/comments?post=' + id);
      dispatch(fetchCommentsSuccess(response.data));
    } catch (e) {
      dispatch(fetchCommentsFailure());
      NotificationManager.error('Could not fetch threads')
    }
  }
};

export const createComments = (commentData, token) => {
  return async dispatch => {
    try {
      await axiosApi.post('/comments', commentData, {headers: {'Authorization': token}});
      dispatch(fetchComments(commentData.post));
    } catch (e) {
      NotificationManager.error('Could not post your news');
      dispatch(fetchCommentsFailure());
    }

  };
};