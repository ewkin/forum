import {
  FETCH_POST_SUCCESS,
  FETCH_POSTS_FAILURE,
  FETCH_POSTS_REQUEST,
  FETCH_POSTS_SUCCESS
} from "../actions/postsActions";


const initialState = {
  posts: [],
  post: [],
  postsLoading: false,
  postsError: null
};

const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POSTS_REQUEST:
      return {...state, productsLoading: true};
    case FETCH_POSTS_SUCCESS:
      return {...state, posts: action.posts, productsLoading: false};
    case FETCH_POST_SUCCESS:
      return {...state, post: action.post, productsLoading: false};
    case FETCH_POSTS_FAILURE:
      return {...state, productsLoading: false, postsError: action.error};
    default:
      return state;
  }
};

export default postsReducer;