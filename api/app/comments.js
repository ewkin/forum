const express = require('express');
const Comment = require('../models/Comment');
const User = require("../models/User");
const router = express.Router();


router.get('/', async (req, res) => {
  try {
    let comments = await Comment.find({post: req.query.post}).populate('user', 'username');

    if (comments) {
      res.send(comments);
    } else {
      res.sendStatus(404);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', async (req, res) => {
  try {
    const token = req.get('Authorization');
    if (!token) {
      return res.status(401).send({error: 'No token password'});
    }
    const user = await User.findOne({token});
    if (!user) {
      return res.status(401).send({error: 'Wrong token'});
    }
    const commentData = req.body;
    commentData.user = user._id;

    const comment = new Comment(commentData);
    await comment.save();
    res.send({comment})
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;