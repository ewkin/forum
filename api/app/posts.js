const path = require('path');
const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const Post = require('../models/Post');
const User = require("../models/User");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {

    const posts = await Post.find().sort({datetime: -1}).populate('user', 'username');
    res.send(posts);
  } catch (e) {
    res.sendStatus(500);
  }
});


router.get('/:id', async (req, res) => {
  try {
    const post = await Post.findOne({_id: req.params.id}).populate('user', 'username');
    if (post) {
      res.send([post]);
    } else {
      res.sendStatus(404);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', upload.single('image'), async (req, res) => {
  try {
    const token = req.get('Authorization');
    if (!token) {
      return res.status(401).send({error: 'No token password'});
    }
    const user = await User.findOne({token});
    if (!user) {
      return res.status(401).send({error: 'Wrong token'});
    }
    const postData = req.body;
    postData.user = user._id;
    if (req.file) {
      postData.image = req.file.filename;
    }
    const post = new Post(postData);
    await post.setDatetime();
    await post.save();
    res.send({post})
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;



