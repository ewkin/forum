const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: function () {
      return this.image === '';
    }
  },
  image: {
    type: String,
    required: function () {
      return this.description === '';
    }
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  datetime: {
    type: Date,
    required: true
  }
});

PostSchema.methods.setDatetime = function () {
  this.datetime = new Date();
};


const Post = mongoose.model('Post', PostSchema);

module.exports = Post;